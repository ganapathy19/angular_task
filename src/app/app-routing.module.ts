import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';

import { DashboardComponent } from './dashboard/dashboard.component';
// import { FormsComponent } from './forms/forms.component';
import { ButtonsComponent } from './buttons/buttons.component';
import { WebsiteComponent } from './website/website.component';
import { LoginComponent } from './login/login.component';

const routes: Routes = [
  // DASHBOARD
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'register', component: WebsiteComponent },
  { path: 'login', component: LoginComponent },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'buttons', component: ButtonsComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true }), CommonModule],
  exports: [RouterModule, CommonModule]
})
export class AppRoutingModule { }
