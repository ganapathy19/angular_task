import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject, Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from '../environments/environment.prod';

@Injectable({
  providedIn: 'root'
})
export class ServiceService {

  // DECLARATIONS
  public static API_URL = environment.apiUrl;
  public API_IMAGE = environment.image_url;

  tabsConfig = new Subject<any>();
  id: number;
  public error: Subject<any> = new Subject<any>();
  public val: Subject<any> = new Subject<any>();

  constructor(private http: HttpClient) {
    this.error.subscribe(result => this.val.next(result));
   }

   // ======================================================================
  // POST DATA
  post_data(particle_url, obj): Observable<any> {
    return this.http.post(ServiceService.API_URL + particle_url, obj);
  }
  // ======================================================================
  // GET LIST WITH PAGINATION
  get_list_data(particle_url, items_per_page, current_page_no): Observable<any> {
    return this.http.get(ServiceService.API_URL + particle_url + '?items_per_page=' + items_per_page + '&current_page_no=' + current_page_no);
  }
  // ======================================================================
  // GET LIST WITH PAGINATION & SEARCH
  get_list_data_PS(particle_url, items_per_page, current_page_no, search): Observable<any> {
    return this.http.get(ServiceService.API_URL + particle_url + '?items_per_page=' + items_per_page + '&current_page_no=' + current_page_no + '&search=' + search);
  }
  // ======================================================================
  // GET LIST
  get_list(particle_url): Observable<any> {
    return this.http.get(ServiceService.API_URL + particle_url);
  }
  // ======================================================================
  // STATUS CHANGE
  status_change(particle_url, id, status): Observable<any> {
    return this.http.get(ServiceService.API_URL + particle_url + '/create?id=' + id + '&status=' + status);
  }
  // ======================================================================
  // GET SINGLE DATA
  get_data(particle_url, id): Observable<any> {
    return this.http.get(ServiceService.API_URL + particle_url + '/' + id + '/edit	');
  }
  // ======================================================================
  // GET SINGLE DATA Create
  get_data_create(particle_url): Observable<any> {
    return this.http.get(ServiceService.API_URL + particle_url + '/create');
  }
  // ======================================================================

  // GET SINGLE DATA
  get_data_show(particle_url, id): Observable<any> {
    return this.http.get(ServiceService.API_URL + particle_url + '/' + id);
  }
  // GET SINGLE DATA
  get_list_show(particle_url, id): Observable<any> {
    return this.http.get(ServiceService.API_URL + particle_url + '?id=' + id);
  }
  // ======================================================================
  // UPDATE DATA
  put_data(particle_url, id, obj): Observable<any> {
    const api_url = ServiceService.API_URL + particle_url;
    const url = `${api_url}/${id}`;
    return this.http.put(url, obj);
  }

  // ======================================================================
  // DELETE DATA
  delete_data(particle_url, id): Observable<any> {
    const api_url = ServiceService.API_URL + particle_url;
    const url = `${api_url}/${id}`;
    return this.http.delete(url);
  }
  
}
