import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbModal, ModalDismissReasons, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ServiceService } from '../service.service';
import { DatePipe } from '@angular/common';
import { ViewEncapsulation } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-buttons',
  templateUrl: './buttons.component.html',
  styleUrls: ['./buttons.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [DatePipe]
})
export class ButtonsComponent implements OnInit {
  model = 1;
  checkboxModel = {
   left: true,
   middle: false,
   right: false
 };
  constructor(private formBuilder: FormBuilder, public service: ServiceService,
    private datePipe: DatePipe, private modalService: NgbModal, public Toastr: ToastrService) { }

  maxDate = new Date().toISOString().split('T')[0];

  Api_Image: String = this.service.API_IMAGE;
  CreateUserForm: FormGroup;
  submitted = false;
  public show1 = false;
  searchText: any = null;
  updated = false;
  ApiUrl: String = 'user_manage';
  remove_data: any;
  UserRemoveURL: String = 'user_remove';
  GetUsersList: any = [];
  ViewUrl: any;
  ViewDetail: any = [];
  Err: any;
  Search: String;
  minDate: any;
  // FILE UPLOAD
  image_append: any;
  imagePreview: any;
  // PAGINATION
  ItemsPerPage: Number = 5;
  Page: Number = 1;
  TotalPage: Number;
  OptionList: any = [5, 10, 50];
  Option = 5;

  ngOnInit() {
    this.onCreateUserFormInit();
    this.getList(`${this.ApiUrl}?items_per_page=${this.ItemsPerPage}&current_page_no=${this.Page}`);
  }

  onDOJChange(event) {
    console.log(event.target.value);
    this.minDate = event.target.value;
  }

  //  *********************************** FUNCTIONS & METHODS ***********************************
  // PAGINATION
  onChangeNextPage = (ev) => {
    this.Page = ev;
    this.getList(`${this.ApiUrl}?items_per_page=${this.ItemsPerPage}&current_page_no=${this.Page}`);
  }
  onChangeItemsPerPage = (ev) => {
    this.ItemsPerPage = ev.target.value;
    this.getList(`${this.ApiUrl}?items_per_page=${this.ItemsPerPage}&current_page_no=${this.Page}`);
  }
  // =====================================================================================
  // SEARCH
  onSearch = (ev) => {
    this.getList(`${this.ApiUrl}?items_per_page=${this.ItemsPerPage}&current_page_no=${this.Page}&search=${ev}`);
  }

  // =====================================================================================
  // INITIATE FORM
  onCreateUserFormInit = () => {
    this.CreateUserForm = this.formBuilder.group({
      id: [''],
      name: ['', Validators.required],
      email: ['', Validators.required],
      d_o_j: ['', Validators.required],
      d_o_r: [''],
      still_wrk: [''],
      profile: ['', Validators.required],
    });
  }

  getAll() { this.show1 = true; this.onReset(); }
  getAll1() { this.show1 = false; this.onReset(); }

  get f() { return this.CreateUserForm.controls; }

  // ==============================================================================================================
  // GET EMPLOYEE LIST
  getList = (url) => {
    this.service.get_list(url)
      .subscribe((result) => {
        if (result.status === 'success') {
          this.GetUsersList = result.data;
          this.TotalPage= result.total_record_count;
        } else {
          console.log('Error');
        }
      });
  }

  // =====================================================================================
  // STILL WORK CHANGE EVENT
  onStiilWrkChange(event) {
    console.log(event.target.checked);
    if (event.target.checked == true) {
      this.CreateUserForm.get('d_o_r').disable();
      this.CreateUserForm.get('d_o_r').setValue('');
    } else {
      this.CreateUserForm.get('d_o_r').enable();
    }
  }

  // =====================================================================================
  // FILE UPLOAD
  onFileChange(event) {
    const reader = new FileReader();
    if (event.target.files && event.target.files.length > 0) {
        const file = event.target.files[0];
        reader.readAsDataURL(file);
        reader.onload = () => {
            this.CreateUserForm.get('profile').setValue(file, { emitModelToViewChange: false });
            this.image_append = file;
            this.imagePreview = reader.result;
        };
    }
  }

  // =====================================================================================
  // SUBMIT
  onSubmit() {
    this.submitted = true;
    console.log(this.CreateUserForm.value);
    // stop here if form is invalid
    if (this.CreateUserForm.invalid) {
      return;
    }
    // if (this.CreateUserForm.valid) {
      console.log(this.CreateUserForm.value);

      const formData: any = new FormData();
        formData.append('id', this.CreateUserForm.value.id);
        formData.append('name', this.CreateUserForm.value.name);
        formData.append('email', this.CreateUserForm.value.email);
        formData.append('d_o_j', this.CreateUserForm.value.d_o_j);
        // formData.append('d_o_r', this.CreateUserForm.value.d_o_r);
        formData.append('still_wrk', this.CreateUserForm.value.still_wrk);
        formData.append('profile', this.image_append);
        formData.append('profile_str', null);

        if(this.CreateUserForm.value.still_wrk == true) {
          formData.append('d_o_r', null);
        } else {
          formData.append('d_o_r', this.CreateUserForm.value.d_o_r);
        }

      this.service.post_data(this.ApiUrl, formData)
        .subscribe((result) => {
          if (result.status === 'success') {
            this.getList(`${this.ApiUrl}?items_per_page=${this.ItemsPerPage}&current_page_no=${this.Page}`);
            this.onReset();
            this.Toastr.success(result.message, result.status);
            this.show1 = false;
          } else {
            this.Err = Object.keys(result.errors).map(key => (result.errors[key]));
            this.Err.forEach(childObj => {
              this.Toastr.error(childObj[0], result.message);
            });
          }
        });

    // }
  }
  imagePreviewString: any;
  // =====================================================================================
  // EDIT
  onEdit = (id: any) => {
    this.service.get_data(this.ApiUrl, id)
      .subscribe((result) => {

        if (result.status === 'success') {
          this.CreateUserForm.patchValue({
            id: result.data.id,
            name: result.data.name,
            email: result.data.email,
            d_o_j: result.data.d_o_j,
            d_o_r: result.data.d_o_r,
            still_wrk: null,
            profile: null,
          });
          if(result.data.still_wrk == 'false') {
            this.CreateUserForm.get('still_wrk').setValue(false);
          }
          if(result.data.still_wrk == 'true') {
            this.CreateUserForm.get('still_wrk').setValue(true);
          }
          this.imagePreview = this.Api_Image + result.data.profile;
          this.imagePreviewString = result.data.profile;
          console.log(this.imagePreview, this.image_append, this.CreateUserForm.value);
          this.CreateUserForm.get('profile').setValue(this.imagePreview, { emitModelToViewChange: false });
        }
        else {
          this.Err = Object.keys(result.errors).map(key => (result.errors[key]));
          this.Err.forEach(childObj => {
            this.Toastr.error(childObj[0], result.message);
          });
        }
        // this.UpdateForm = true;
      });
    this.show1 = true;
    this.updated = true;
  }
  // =====================================================================================
  // UPDATE
  onUpdate = () => {
    this.submitted = true;
    if (this.CreateUserForm.invalid) {
      return;
    }
      console.log(this.CreateUserForm.value);
      const formData: any = new FormData();
        formData.append('id', this.CreateUserForm.value.id);
        formData.append('name', this.CreateUserForm.value.name);
        formData.append('email', this.CreateUserForm.value.email);
        formData.append('d_o_j', this.CreateUserForm.value.d_o_j);
        formData.append('still_wrk', this.CreateUserForm.value.still_wrk);
        formData.append('profile', this.image_append);

        if(this.CreateUserForm.value.still_wrk == true) {
          formData.append('d_o_r', '');
        } else {
          formData.append('d_o_r', this.CreateUserForm.value.d_o_r);
        }
        if (this.image_append == undefined) {
          formData.append('profile_str', this.imagePreviewString);
        } 

    this.service.post_data(this.ApiUrl, formData)
    // this.service.put_data(this.ApiUrl, this.CreateUserForm.value.id, this.CreateUserForm.value)
      .subscribe((result) => {
        if (result.status === 'success') {
          this.Toastr.success(result.message, result.status);
          this.getList(`${this.ApiUrl}?items_per_page=${this.ItemsPerPage}&current_page_no=${this.Page}`);
          this.onReset();
          this.show1 = false;
        } else {
          this.Err = Object.keys(result.errors).map(key => (result.errors[key]));
          this.Err.forEach(childObj => {
            this.Toastr.error(childObj[0], result.message);
          });
        }
      });

  }

  // =====================================================================================
  // MODAL DISMISS REASON
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  // =====================================================================================
  // VIEW POPUP OPEN
  onViewUser = (ViewModal, id) => {
    this.ViewUrl = this.ApiUrl + '/' + id;
    this.service.get_list(this.ViewUrl)
      .subscribe((result) => {
        if (result.status === 'success') {
          this.ViewDetail = result.data;
          console.log(this.ViewDetail);
          this.modalService.open(ViewModal).result.then(
            model_result => {
              const closeResult = `Closed with: ${model_result}`;
              console.log(closeResult);
            },
            reason => {
              const closeResult = `Dismissed ${this.getDismissReason(reason)}`;
              console.log(closeResult);
            }
          );
        }
      });
  }

  // =====================================================================================
  // REMOVE POPUP
  // STEP1: POPUP OPEN
  onRemovePop = (RemoveModel, data) => {
    this.remove_data = data;
    this.modalService.open(RemoveModel).result.then(
      model_result => {
        const closeResult = `Closed with: ${model_result}`;
        console.log(closeResult);
      },
      reason => {
        // console.log(closeResult);
      }
    );
  }
  // // STEP2: REMOVE DATA
  onRemove = (data, id) => {
    this.service.get_list(`${this.UserRemoveURL}?user_id=${id}`)
      .subscribe((result) => {
        if (result.status === 'success') {
          // this.Toastr.success(result.message, result.status);
          this.getList(`${this.ApiUrl}?items_per_page=${this.ItemsPerPage}&current_page_no=${this.Page}`);
        }
      });
  }

  // =====================================================================================
  // FORM CLEAR
  onReset() {
    this.submitted = false;
    this.updated = false;
    this.imagePreview = null;
    this.image_append = null;
    this.imagePreviewString = null;
    this.minDate = null;
    this.CreateUserForm.reset();
  }

}
